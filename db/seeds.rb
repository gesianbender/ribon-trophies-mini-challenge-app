# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
Monster.create(
	[
		{ name: 'Turtle' }, 
		{ name: 'Bowser' }
	]
)

User.create(
	[
		{ name: 'Gê Bender' }, 
		{ name: 'Ribon' }, 
		{ name: 'Juju' }
	]
)

Category.create(
	[
		{ name: 'Coins' }, 
		{ name: 'Kills' },
		{ name: 'Deaths' }
	]
)

Level.create(
	[
		{ category_id: 1, name: 'NUB', count: 1, order: 1 }, 
		{ category_id: 1, name: 'Kiddo', count: 100, order: 2 }, 
		{ category_id: 1, name: 'Prof', count: 1000, order: 3 }, 
		{ category_id: 1, name: 'King', count: 10000, order: 4 }, 
		{ category_id: 1, name: 'God', count: 100000, order: 5 },
		{ category_id: 2, name: 'NUB', count: 1, order: 1 }, 
		{ category_id: 2, name: 'Kiddo', count: 100, order: 2 }, 
		{ category_id: 2, name: 'Prof', count: 1000, order: 3 }, 
		{ category_id: 2, name: 'King', count: 10000, order: 4 }, 
		{ category_id: 2, name: 'God', count: 100000, order: 5 },
		{ category_id: 3, name: 'NUB', count: 1, order: 1 }, 
		{ category_id: 3, name: 'Kiddo', count: 10, order: 2 }, 
		{ category_id: 3, name: 'Prof', count: 25, order: 3 }, 
		{ category_id: 3, name: 'King', count: 50, order: 4 }, 
		{ category_id: 3, name: 'God', count: 100, order: 5 }
	]
)