class CreateMonsterCounters < ActiveRecord::Migration[5.2]
  def change
    create_table :monster_counters do |t|
      t.references :user, foreign_key: true
      t.references :monster, foreign_key: true
      t.integer :count, :null => false, :default => 0

      t.timestamps
    end
  end
end
