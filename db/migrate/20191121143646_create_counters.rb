class CreateCounters < ActiveRecord::Migration[5.2]
  def change
    create_table :counters do |t|
      t.references :user, foreign_key: true
      t.references :category, foreign_key: true
      t.integer :count, :null => false, :default => 0

      t.timestamps
    end
  end
end
