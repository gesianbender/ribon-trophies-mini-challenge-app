class CreateLevels < ActiveRecord::Migration[5.2]
  def change
    create_table :levels do |t|
      t.references :category, foreign_key: true
      t.string :name
      t.integer :count
      t.integer :order

      t.timestamps
    end
  end
end
