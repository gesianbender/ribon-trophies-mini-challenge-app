# Ribon Trophies Mini Challenge App

- Desenvolvido em Ruby on Rails + Postgres
- A abordagem foi a de fazer uma única API para inserir e ler os dados, seja do sistema base, quanto dos troféus;
- A verificação pode ser feita através do Postman ou similar;
- Também poderá ser verificado através da execução de testes, pelo próprio Rails;
- Utilizamos uma aplicação Rails --API, ou seja, sem a interface gráfica e a lógica escrita em seus controllers, models e helpers respeitando a estrutura e modelagem padrão deste Framework. Basicamente utilizei um callback no controller para disparar a ação de verificar e dar os troféus sempre que uma das tabelas transacionais é incrementada. Quanto ao cuidado com o desempenho, mantive a estrutura relacoinal de chaves e indexes gerados pelo próprio rails, conforme pode ser conferido nas migrations.

## Instalação em ambiente de desenvolvimento

Considerando que você tem instalado em sem ambiente:
- Ruby 2.6 ou superior
- Rails 5.2 ou superior
- Postgres (psql) 11.5 ou superior

1. Clone o repositório:
`$ git clone https://gitlab.com/gesianbender/ribon-trophies-mini-challenge-app.git`

2. Acesse o folder
`$ cd ribon-trophies-mini-challenge-app/`

3. Crie um banco de dados para o ambiente de desenvolvimento
4. Crie um arquivo em config/database.yml contendo as configurações do seus bancos de dados

		default: &default
			adapter: postgresql
			encoding: unicode
			pool: <%= ENV.fetch("RAILS_MAX_THREADS") { 5 } %>
			host: localhost
			username: ribon
			password: ribon
			timeout: 5000
		
		development:
			<<: *default
			database: ribon_trophies_development

		test:
			<<: *default
			database: ribon_trophies_test

5. Execute as migrations para criar as tabelas
`$ rails db:migrate`

6. Execute as migrations para criar as tabelas em ambiente de teste
`$ rails db:migrate RAILS_ENV=test`

7. Popule o banco de dados com os dados básicos (usuários e monstros)
`$ rails db:seed`

8. "Levante" o servidor
`$ rails s`

## API e endpoints via Postman

### Usuários:
##### Listar: `GET localhost:3000/api/v1/users`
##### Mostrar um registro:  `GET localhost:3000/api/v1/users/1`
##### Total de moedas coletadas de um usuário: `GET localhost:3000/api/v1/users/sum_coins/1`
##### Total de monstros mortos por um usuário: `GET localhost:3000/api/v1/users/count_kills/1`
##### Total de mortes de um usuário: `GET localhost:3000/api/v1/users/count_deaths/1`

------------


#### Monstros:
##### Listar: `GET localhost:3000/api/v1/monsters`
##### Mostrar um registro: `GET localhost:3000/api/v1/monsters/1`

------------

#### Moedas coletadas
##### Inserir: `POST localhost:3000/api/v1/collected_coins`
    # Content-Type: application/json; 
	# body:
	{
		"user_id": "1",
		"value": "10"
	}
##### Listar: `GET localhost:3000/api/v1/collected_coins`
##### Mostrar um registro: `GET localhost:3000/api/v1/collected_coins/1`
##### Listar registros de um usuário: `GET localhost:3000/api/v1/collected_coins/user/1`

------------
#### Monstros mortos
##### Inserir: `POST localhost:3000/api/v1/killed_monsters`
    # Content-Type: application/json; 
	# body:
	{
		"user_id": "1",
		"monster_id": "1"
	}
##### Listar: `GET localhost:3000/api/v1/killed_monsters`
##### Mostrar um registro: `GET localhost:3000/api/v1/killed_monsters/1`
##### Listar registros de um usuário: `GET localhost:3000/api/v1/killed_monsters/user/1`

------------
#### Mortes
##### Inserir: `POST localhost:3000/api/v1/deaths`
    # Content-Type: application/json; 
	# body:
	{
		"user_id": "1"
	}
##### Listar: `GET localhost:3000/api/v1/deaths`
##### Mostrar um registro: `GET localhost:3000/api/v1/deaths/1`
##### Listar registros de um usuário: `GET localhost:3000/api/v1/deaths/user/1`
------------
### Troféus
#### Listar todos os troféus de um usuário: `GET localhost:3000/api/v1/trophies/1`
<!-- #### Listar troféus de moedas coletadas de um usuário: `GET localhost:3000/api/v1/trophies/coins/1`
#### Listar troféus de mortes de monstros de um usuário: `GET localhost:3000/api/v1/trophies/kills/1`
#### Listar troféus de mortes de um usuário: `GET localhost:3000/api/v1/trophies/deaths/1` -->
------------
## Verificando cobertura de testes
`$ rails t`

    Finished in 0.674282s, 63.7716 runs/s, 161.6535 assertions/s.
    43 runs, 109 assertions, 0 failures, 0 errors, 0 skips

### Fim