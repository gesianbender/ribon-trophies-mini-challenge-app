require 'test_helper'

class KilledMonstersControllerTest < ActionDispatch::IntegrationTest
  
  test "should restore list" do
    get api_v1_killed_monsters_path
    assert_response :success

		list = JSON.parse(@response.body)
		assert_equal 3, list['data'].count
  end

  test "should restore user list" do
    user = users(:user2)
    get api_v1_user_killed_monsters_path user_id: user.id
    assert_response :success

    monster1 = monsters(:monster1)
    monster2 = monsters(:monster2)

    response = JSON.parse(@response.body)
    assert_equal 2, response['data'].count
    assert_equal monster1.id, response['data'].first['monster_id']
    assert_equal monster2.id, response['data'].second['monster_id']
  end

  test "should restore single row" do
  	row = killed_monsters(:one)
  	user = users(:user1)
  	monster = monsters(:monster1)

    get api_v1_killed_monster_path id: row.id
    assert_response :success

    response = JSON.parse(@response.body)
    assert_equal user.id, response['data']['user_id']
    assert_equal monster.id, response['data']['monster_id']
  end

  test "should create" do
  	user = users(:user1)
  	monster = monsters(:monster1)
  	rows = KilledMonster.count

  	new_row = {user_id: user.id, monster_id: monster.id}
    post api_v1_killed_monsters_path, params: new_row.to_json, headers: { 'CONTENT_TYPE' => 'application/json' }
    
    assert_response :success
    assert_equal rows+1, KilledMonster.count
  end

end