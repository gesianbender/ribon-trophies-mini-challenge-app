require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest
  
  test "should restore list" do
    get api_v1_users_path
    assert_response :success

		list = JSON.parse(@response.body)
		assert_equal 3, list['data'].count
  end

  test "should restore single row" do
  	row = users(:user1)

    get api_v1_user_path id: row.id
    assert_response :success

    response = JSON.parse(@response.body)
    assert_equal 'nameUser1', response['data']['name']
  end

  test "should restore coins sum" do
    row = users(:user2)

    get api_v1_user_sum_coins_path user_id: row.id
    assert_response :success

    response = JSON.parse(@response.body)
    assert_equal 11, response['data']
  end

  test "should restore kills count" do
    row = users(:user2)

    get api_v1_user_count_kills_path user_id: row.id
    assert_response :success

    response = JSON.parse(@response.body)
    assert_equal 2, response['data']
  end

  test "should restore deaths count" do
    row = users(:user2)

    get api_v1_user_count_deaths_path user_id: row.id
    assert_response :success

    response = JSON.parse(@response.body)
    assert_equal 2, response['data']
  end

end