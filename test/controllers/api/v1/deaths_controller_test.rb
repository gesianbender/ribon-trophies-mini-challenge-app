require 'test_helper'

class DeathsControllerTest < ActionDispatch::IntegrationTest
  
  test "should restore list" do
    get api_v1_deaths_path
    assert_response :success

		list = JSON.parse(@response.body)
		assert_equal 3, list['data'].count
  end

  test "should restore user list" do
    user = users(:user2)
    get api_v1_user_deaths_path user_id: user.id
    assert_response :success

    response = JSON.parse(@response.body)
    assert_equal 2, response['data'].count
  end

  test "should restore single row" do
  	row = deaths(:one)
  	user = users(:user1)

    get api_v1_death_path id: row.id
    assert_response :success

    response = JSON.parse(@response.body)
    assert_equal user.id, response['data']['user_id']
  end

  test "should create" do
  	user = users(:user1)
  	rows = Death.count

  	new_row = {user_id: user.id}
    post api_v1_deaths_path, params: new_row.to_json, headers: { 'CONTENT_TYPE' => 'application/json' }
    
    assert_response :success
    assert_equal rows+1, Death.count
  end

end