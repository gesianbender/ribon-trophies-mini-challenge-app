require 'test_helper'

class CollectedCoinsControllerTest < ActionDispatch::IntegrationTest
  
  test "should restore list" do
    get api_v1_collected_coins_path
    assert_response :success

		coins = JSON.parse(@response.body)
		assert_equal 3, coins['data'].count
  end

  test "should restore user list" do
    user = users(:user2)
    get api_v1_user_collected_coins_path user_id: user.id
    assert_response :success

    coins = JSON.parse(@response.body)
    assert_equal 2, coins['data'].count
    assert_equal 1, coins['data'].first['value']
    assert_equal 10, coins['data'].second['value']
  end

  test "should restore single row" do
  	collected_coin = collected_coins(:one)
  	user = users(:user1)

    get api_v1_collected_coin_path id: collected_coin.id
    assert_response :success

    coin = JSON.parse(@response.body)
    assert_equal user.id, coin['data']['user_id']
    assert_equal 123, coin['data']['value']
  end

  test "should create" do
  	user = users(:user1)
  	coins = CollectedCoin.count

  	new_coin_collected = {user_id: user.id, value: 1}
    post api_v1_collected_coins_path, params: new_coin_collected.to_json, headers: { 'CONTENT_TYPE' => 'application/json' }
    
    assert_response :success
    assert_equal coins+1, CollectedCoin.count
  end

end