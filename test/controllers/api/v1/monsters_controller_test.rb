require 'test_helper'

class MostersControllerTest < ActionDispatch::IntegrationTest
  
  test "should restore list" do
    get api_v1_monsters_path
    assert_response :success

		list = JSON.parse(@response.body)
		assert_equal 2, list['data'].count
  end

  test "should restore single row" do
  	row = monsters(:monster1)

    get api_v1_monster_path id: row.id
    assert_response :success

    response = JSON.parse(@response.body)
    assert_equal 'monsterName1', response['data']['name']
  end

end