require 'test_helper'

class TrophiesControllerTest < ActionDispatch::IntegrationTest
  
  test "should restore list" do
    get api_v1_user_trophies_path user_id: users(:user1).id
    assert_response :success

		list = JSON.parse(@response.body)
		assert_equal 1, list['data'].count
  end

end