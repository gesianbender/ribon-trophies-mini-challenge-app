require 'test_helper'

class UserTest < ActiveSupport::TestCase
	setup do
    @user = users(:user1)
  end

  test "has collected_coins" do
    assert_instance_of CollectedCoin, @user.collected_coins.first
  end

  test "has killed_monsters" do
    assert_instance_of KilledMonster, @user.killed_monsters.first
  end

  test "has deaths" do
    assert_instance_of Death, @user.deaths.first
  end

  test "has trophies" do
    assert_instance_of Trophy, @user.trophies.first
  end

  test "has counters" do
    assert_instance_of Counter, @user.counters.first
  end

  test "should have name" do
  	@user.name = nil
    assert_not @user.valid?
  end

  test "should sum collected coins" do
  	user = users(:user2)
  	assert_equal 11, user.sum_coins
  end

  test "should count killed monsters" do
  	user = users(:user2)
  	assert_equal 2, user.count_kills
  end

  test "should count deaths" do
  	user = users(:user2)
  	assert_equal 2, user.count_deaths
  end

end
