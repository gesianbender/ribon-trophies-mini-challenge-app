require 'test_helper'

class TrophyTest < ActiveSupport::TestCase
	setup do
    @trophy = trophies(:one)
  end

  test "should have user" do
  	trophy = Trophy.new
  	trophy.level = levels(:one)
    assert_not trophy.valid?

    trophy.user = users(:user1)
    assert trophy.valid?
  end

  test "should have level" do
  	trophy = Trophy.new
  	trophy.user = users(:user1)
    assert_not trophy.valid?

    trophy.level = levels(:one)
    assert trophy.valid?
  end

end
