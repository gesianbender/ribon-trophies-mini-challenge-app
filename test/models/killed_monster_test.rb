require 'test_helper'

class KilledMonsterTest < ActiveSupport::TestCase
	setup do
    @killed_monster = killed_monsters(:one)
  end

  test "should have user" do
  	@killed_monster.user = nil

    assert_not @killed_monster.valid?
  end

  test "should have monster" do
    @killed_monster = killed_monsters(:one)
  	@killed_monster.monster = nil

    assert_not @killed_monster.valid?
  end

  test "should increment counter after create" do
    killedMonster = KilledMonster.new(user: users(:user1), monster: monsters(:monster1))
    killedMonster.expects(:count_kills).once

    killedMonster.save    
  end

  test "should increment monster counter after create" do
    killedMonster = KilledMonster.new(user: users(:user1), monster: monsters(:monster1))
    killedMonster.expects(:go_monster_kills_counter).once

    killedMonster.save    
  end

  test "should increment monster counter" do
    counter = MonsterCounter.where(user: @killed_monster.user, monster: @killed_monster.monster).first
    assert_equal 1, counter.count
    
    @killed_monster.go_monster_kills_counter
    counter = MonsterCounter.where(user: @killed_monster.user, monster: @killed_monster.monster).first
    assert_equal 2, counter.count

    @killed_monster.monster = monsters(:monster2)
    @killed_monster.go_monster_kills_counter
    counter = MonsterCounter.where(user: @killed_monster.user, monster: @killed_monster.monster).first
    assert_equal 1, counter.count
  end


end
