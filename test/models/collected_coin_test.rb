require 'test_helper'

class CollectedCoinTest < ActiveSupport::TestCase

	setup do
    @collected_coin = collected_coins(:one)
  end

  test "should have user" do
  	collected_coin = CollectedCoin.new
    collected_coin.value = 10

    assert_not collected_coin.valid?
  end

  test "should have value" do
  	@collected_coin.value = nil
    assert_not @collected_coin.valid?
  end

  test "should increment counter after create" do
    collected_coin = CollectedCoin.new(user: users(:user1), value: 1)
    collected_coin.expects(:count_coins).once
    collected_coin.save
  end
end
