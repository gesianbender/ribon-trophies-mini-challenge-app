require 'test_helper'

class MonsterCounterTest < ActiveSupport::TestCase
	setup do
    @monster_counter = monster_counters(:one)
  end

  test "should have user" do
    monster_counter = monster_counters(:one)
    monster_counter.user = nil

    assert_not monster_counter.valid?
  end

  test "should have monster" do
    monster_counter = monster_counters(:one)
    monster_counter.monster = nil

    assert_not monster_counter.valid?
  end
end
