require 'test_helper'

class CounterTest < ActiveSupport::TestCase

  setup do
    @counter = counters(:one)
  end

  test "should have user" do
    counter = Counter.new
    counter.category = categories(:one)

    assert_not counter.valid?
  end

  test "should have category" do
    counter = Counter.new
    counter.user = users(:user1)

      assert_not counter.valid?
  end

  test "should reward on create" do
    counter = Counter.new(user: users(:user1), category: categories(:one))
    counter.expects(:reward).once
    counter.save
  end

  test "should reward on update" do
    counter = counters(:one)
    counter.count = 100

		counter.expects(:reward).once
    counter.save
  end

  test "should give a trophy when deserve it and haven't yet" do
    user = users(:user3)
    category = categories(:one)
    level = levels(:one)

    trophies = Trophy.exists?(user: user, level: level)
    assert_equal false, trophies
    
    counter = Counter.create(user: user, category: category, count: 1)
    trophies = Trophy.exists?(user: user, level: level)
    assert_equal true, trophies

    counter.count = 2
    counter.save
    trophies = Trophy.where(user: user, level: level).count
    assert_equal 1, trophies

    counter.count = 5
    counter.save
    trophies = Trophy.where(user: user, level: level).count
    assert_equal 1, trophies

  end
end
