require 'test_helper'

class CategoryTest < ActiveSupport::TestCase
  setup do
    @category = categories(:one)
  end

  test "has counters" do
    assert_instance_of Counter, @category.counters.first
  end

   test "has levels" do
    assert_instance_of Level, @category.levels.first
  end
end
