require 'test_helper'

class MonsterTest < ActiveSupport::TestCase
	setup do
    @monster = monsters(:monster1)
  end

  test "has monster counters" do
    assert_instance_of MonsterCounter, @monster.counters.first
  end

  test "has monster killed monsters" do
    assert_instance_of KilledMonster, @monster.killeds.first
  end

  test "should have name" do
  	@monster.name = nil
    assert_not @monster.valid?
  end
end
