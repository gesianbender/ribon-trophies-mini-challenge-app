require 'test_helper'

class CountableTest < ActionView::TestCase

  setup do
    @collected_coin = collected_coins(:one)
    @category = categories(:one)
  end

  test 'should create counter when first count and dont creat after that' do
    @collected_coin.go_counter(@category.id, 1)
    counter = Counter.where(user_id: @collected_coin.user.id).count
    assert_equal 1, counter

    @collected_coin.go_counter(@category.id, 1)
    counter = Counter.where(user_id: @collected_coin.user.id).count
    assert_equal 1, counter

  end

  test 'should increment counter' do
    @collected_coin.go_counter(@category.id, 1)
    counter = Counter.where(user_id: @collected_coin.user.id, category_id: @category.id).first
    assert_equal 2, counter.count

    @collected_coin.go_counter(@category.id, 4)
    counter = Counter.where(user_id: @collected_coin.user.id, category_id: @category.id).first
    assert_equal 6, counter.count
  end

  test "should increment coin" do
    collected_coin = CollectedCoin.new(user: users(:user1), value: 5)
    collected_coin.expects(:go_counter).with(1,5).once
    collected_coin.save    
  end

  test "should increment kill" do
    killedMonster = KilledMonster.new(user: users(:user1), monster: monsters(:monster1))
    killedMonster.expects(:go_counter).with(2,1).once
    killedMonster.save
  end

  test "should increment death" do
    death = Death.new(user: users(:user1))
    death.expects(:go_counter).with(3,1).once
    death.save
  end
end
