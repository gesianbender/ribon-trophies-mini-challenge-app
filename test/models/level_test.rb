require 'test_helper'

class LevelTest < ActiveSupport::TestCase
  setup do
    @level = levels(:one)
  end

  test "should have user" do
  	level = Level.new
    assert_not level.valid?

    level.category = categories(:one)
    assert level.valid?
  end

	test "has trophies" do
    assert_instance_of Trophy, @level.trophies.first
  end

end
