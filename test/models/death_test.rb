require 'test_helper'

class DeathTest < ActiveSupport::TestCase
	setup do
    @death = deaths(:one)
  end

  test "should have user" do
  	@death.user = nil
    assert_not @death.valid?
  end

  test "should increment counter after create" do
    death = Death.new(user: users(:user1))
    death.expects(:count_deaths).once
    death.save
  end
end
