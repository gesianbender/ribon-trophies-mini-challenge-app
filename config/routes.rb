Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  namespace 'api' do
  	namespace 'v1' do
  		resources :users, only: [:index, :show]
      resources :monsters, only: [:index, :show]
      resources :collected_coins, except: [:destroy, :update]
  		resources :killed_monsters, except: [:destroy, :update]
  		resources :deaths, except: [:destroy, :update]
  		
      get '/collected_coins/user/:user_id' => 'collected_coins#user', as: 'user_collected_coins'
      get '/killed_monsters/user/:user_id' => 'killed_monsters#user', as: 'user_killed_monsters'
      get '/deaths/user/:user_id' => 'deaths#user', as: 'user_deaths'

      get '/users/sum_coins/:user_id' => 'users#sum_coins', as: 'user_sum_coins'
      get '/users/count_kills/:user_id' => 'users#count_kills', as: 'user_count_kills'
      get '/users/count_deaths/:user_id' => 'users#count_deaths', as: 'user_count_deaths'
      
      get '/trophies/:user_id' => 'trophies#user', as: 'user_trophies'
  	end
  end
end
