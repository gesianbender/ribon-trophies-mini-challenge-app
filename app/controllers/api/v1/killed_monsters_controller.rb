module Api
	module V1
		class KilledMonstersController < ApplicationController

			def index
				killed_monsters = KilledMonster.order('id DESC');
				render json: {status: 'SUCCESS', message:'Loaded killed monsters', data:killed_monsters},status: :ok
			end

			def user
				killed_monsters = KilledMonster.where(:user => params[:user_id]);
				render json: {status: 'SUCCESS', message:'Loaded user killed monsters', data:killed_monsters},status: :ok
			end

			def show
				killed_monster = KilledMonster.find(params[:id])
				render json: {status: 'SUCCESS', message:'Loaded killed monster', data:killed_monster},status: :ok
			end

			def create
				killed_monster = KilledMonster.new(killed_monster_params)
				if killed_monster.save
					render json: {status: 'SUCCESS', message:'Saved killed monster', data:killed_monster},status: :ok
				else
					render json: {status: 'ERROR', message:'Killed monster not saved', data:killed_mnoster.erros},status: :unprocessable_entity
				end
			end

			private
			def killed_monster_params
				params.permit(:user_id, :monster_id)
			end	
		end
	end
end