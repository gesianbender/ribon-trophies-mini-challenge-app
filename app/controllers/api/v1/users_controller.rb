module Api
	module V1
		class UsersController < ApplicationController

			def index
				users = User.order('id DESC');
				render json: {status: 'SUCCESS', message:'Loaded users', data:users},status: :ok
			end

			def show
				user = User.find(params[:id])
				render json: {status: 'SUCCESS', message:'Loaded user', data:user},status: :ok
			end

			def sum_coins
				user = User.find(params[:user_id])
				render json: {status: 'SUCCESS', message:'Loaded user sum coins', data:user.sum_coins},status: :ok
			end

			def count_kills
				user = User.find(params[:user_id])
				render json: {status: 'SUCCESS', message:'Loaded user kills count', data:user.count_kills},status: :ok
			end

			def count_deaths
				user = User.find(params[:user_id])
				render json: {status: 'SUCCESS', message:'Loaded user deaths count', data:user.count_deaths},status: :ok
			end

		end
	end
end