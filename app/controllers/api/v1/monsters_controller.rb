module Api
	module V1
		class MonstersController < ApplicationController

			def index
				monsters = Monster.order('id DESC');
				render json: {status: 'SUCCESS', message:'Loaded monsters', data:monsters},status: :ok
			end

			def show
				monster = Monster.find(params[:id])
				render json: {status: 'SUCCESS', message:'Loaded monster', data:monster},status: :ok
			end

		end
	end
end