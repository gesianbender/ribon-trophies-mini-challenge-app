module Api
	module V1
		class CollectedCoinsController < ApplicationController

			def index
				collected_coins = CollectedCoin.order('id DESC');
				render json: {status: 'SUCCESS', message:'Loaded collected coins', data:collected_coins},status: :ok
			end

			def user
				collected_coins = CollectedCoin.where(:user => params[:user_id]);
				render json: {status: 'SUCCESS', message:'Loaded user collected coins', data:collected_coins},status: :ok
			end

			def show
				collected_coin = CollectedCoin.find(params[:id])
				render json: {status: 'SUCCESS', message:'Loaded coleected coin', data:collected_coin},status: :ok
			end

			def create
				collected_coin = CollectedCoin.new(collected_coin_params)
				if collected_coin.save
					render json: {status: 'SUCCESS', message:'Saved collected coin', data:collected_coin},status: :ok
				else
					render json: {status: 'ERROR', message:'Collected Coin not saved', data:collected_coin.erros},status: :unprocessable_entity
				end
			end
			
			private
			def collected_coin_params
				params.permit(:user_id, :user, :value)
			end

		end
	end
end