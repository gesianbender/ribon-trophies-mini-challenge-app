module Api
	module V1
		class DeathsController < ApplicationController

			def index
				deaths = Death.order('id DESC');
				render json: {status: 'SUCCESS', message:'Loaded deaths', data:deaths},status: :ok
			end

			def user
				deaths = Death.where(:user => params[:user_id]);
				render json: {status: 'SUCCESS', message:'Loaded user deaths', data:deaths},status: :ok
			end

			def show
				death = Death.find(params[:id])
				render json: {status: 'SUCCESS', message:'Loaded Death', data:death},status: :ok
			end

			def create
				death = Death.new(death_params)
				if death.save
					render json: {status: 'SUCCESS', message:'Saved death', data:death},status: :ok
				else
					render json: {status: 'ERROR', message:'death not saved', data:death.erros},status: :unprocessable_entity
				end
			end

			private
			def death_params
				params.permit(:user_id)
			end	
		end
	end
end