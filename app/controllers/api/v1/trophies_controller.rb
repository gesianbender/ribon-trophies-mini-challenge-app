module Api
	module V1
		class TrophiesController < ApplicationController

			def user
				trophies = Trophy.where(user_id: params[:user_id])
				render json: {status: 'SUCCESS', message:'Loaded user trophies', data:trophies},status: :ok
			end
		end
	end
end