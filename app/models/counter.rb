class Counter < ApplicationRecord
  belongs_to :user
  belongs_to :category

  after_save :reward

  private
  def reward
  	level = category.levels.where('count <= ?', count).order(order: :desc).first
  	hasnt_trophy = !user.trophies.exists?(level: level)
  	Trophy.create(user: user, level: level) if hasnt_trophy == true
  end
end
