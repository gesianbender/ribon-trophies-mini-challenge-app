class CollectedCoin < ApplicationRecord
	include Countable

	belongs_to :user
  validates :value, presence: true

	after_create :count_coins

	private

	def count_coins
		go_counter(1, self.value)
	end
	
end
