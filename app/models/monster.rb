class Monster < ApplicationRecord
	has_many :counters, class_name: :MonsterCounter
	has_many :killeds, class_name: :KilledMonster

	validates :name, presence: true
end
