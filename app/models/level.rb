class Level < ApplicationRecord
  belongs_to :category
  has_many :trophies
end
