module Countable
	include ActiveSupport::Concern
	
	def go_counter(category_id, increment)
		counter = Counter.find_or_create_by(category_id: category_id, user_id: self.user.id)
		counter.count += increment
		counter.save
	end

end
