class User < ApplicationRecord
	has_many :collected_coins
	has_many :killed_monsters
	has_many :deaths
	has_many :trophies
	has_many :counters

	validates :name, presence: true

	def sum_coins
    collected_coins.sum(:value)
  end

	def count_kills
    killed_monsters.count
  end

	def count_deaths
    deaths.count
  end

end
