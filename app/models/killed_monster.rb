class KilledMonster < ApplicationRecord
	include Countable

  belongs_to :user
  belongs_to :monster

  after_create :count_kills

	def go_monster_kills_counter
		counter = MonsterCounter.find_or_create_by(user: user, monster: monster)
		counter.count += 1
		counter.save
	end

	private
	def count_kills
		go_counter(2, 1)
		go_monster_kills_counter
	end

end
