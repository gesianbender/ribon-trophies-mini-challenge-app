class Death < ApplicationRecord
	include Countable

  belongs_to :user
  after_create :count_deaths

  private

	def count_deaths
		go_counter(3, 1)
	end
end
