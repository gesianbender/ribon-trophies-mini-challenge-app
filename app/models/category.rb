class Category < ApplicationRecord
	has_many :counters
	has_many :levels
end
